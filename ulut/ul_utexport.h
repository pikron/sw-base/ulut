/* Shared library support */
#ifndef UL_EXPORT
  #if defined(_WIN32)&&defined(_MSC_VER)
    #define UL_DLL_IMPORT __declspec(dllimport)
    #define UL_DLL_EXPORT __declspec(dllexport)
    #define UL_DLL_LOCAL
    #define UL_DLL_PUBLIC
  #else
    #if (__GNUC__ >= 4) && !defined(GCC_HASCLASSVISIBILITY)
       #define GCC_HASCLASSVISIBILITY
    #endif
    #define UL_DLL_IMPORT
    #ifdef GCC_HASCLASSVISIBILITY
      #define UL_DLL_EXPORT __attribute__ ((visibility("default")))
      #define UL_DLL_LOCAL __attribute__ ((visibility("hidden")))
      #define UL_DLL_PUBLIC __attribute__ ((visibility("default")))
    #else
      #define UL_DLL_EXPORT
      #define UL_DLL_LOCAL
      #define UL_DLL_PUBLIC
    #endif
  #endif
#endif /* UL_EXPORT */

/* Define UL_UTAPI for DLL builds */
#ifdef UL_UTDLL
  #ifdef UL_UTDLL_EXPORTS
    #define UL_UTAPI UL_EXPORT
  #else
    #define UL_UTAPI  UL_IMPORT
  #endif /* UL_UTDLL_EXPORTS */
#else
  #define UL_API
#endif /* UL_UTDLL */

/* Throwable classes must always be visible on GCC in all binaries */
#if defined(_WIN32)&&defined(_MSC_VER)
  #define UL_EXCEPTIONAPI(api) api
#elif defined(GCC_HASCLASSVISIBILITY)
  #define UL_EXCEPTIONAPI(api) UL_EXPORT
#else
  #define UL_EXCEPTIONAPI(api)
#endif

/* Other possible solution is to use pragmas */
/*
 * #pragma GCC visibility push(hidden)
 * extern void someprivatefunct(int);
 * #pragma GCC visibility pop
 */

/* GCC switches to handle visibility */
/*
 * -fvisibilty=hidden
 * -fvisibility-inlines-hidden
 */