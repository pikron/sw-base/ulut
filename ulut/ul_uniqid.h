/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_uniqid.h  - unique ID generator

  (C) Copyright 2003-2004 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_UNIQID_H
#define _UL_UNIQID_H

#include "ul_gavl.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long ul_uniqid_t;

typedef struct ul_uniqid_range_t {
  ul_uniqid_t first;
  ul_uniqid_t last;
} ul_uniqid_range_t;

/**
 * struct ul_uniqid_pool_t - The Unique Identifiers Pool
 * @items:	GAVL tree of not allocated yet ranges
 * @range:	numeric range to allocate IDs from
 *
 * The unique pool provides functions to manage unique numerical IDs.
 * The pool is first initialized by function ul_uniqid_pool_init().
 * The available range is specified at this time. The pool can be
 * flushed and destroyed by call ul_uniqid_pool_done().
 *
 * The function ul_uniqid_pool_alloc_one() returns first free ID from
 * range. The ID is returned to the pool by function ul_uniqid_pool_free_one().
 * There are even functions to reserve and release specific IDs range.
 */
typedef struct ul_uniqid_pool_t {
  gavl_fles_int_root_field_t items;
  ul_uniqid_range_t range;
} ul_uniqid_pool_t;

int ul_uniqid_pool_init(ul_uniqid_pool_t *pool, ul_uniqid_t first, ul_uniqid_t last);
int ul_uniqid_pool_done(ul_uniqid_pool_t *pool);
int ul_uniqid_pool_reserve(ul_uniqid_pool_t *pool, ul_uniqid_t first, ul_uniqid_t last);
int ul_uniqid_pool_release(ul_uniqid_pool_t *pool, ul_uniqid_t first, ul_uniqid_t last);
int ul_uniqid_pool_alloc_one(ul_uniqid_pool_t *pool, ul_uniqid_t *ptrid);
int ul_uniqid_pool_alloc_one_after(ul_uniqid_pool_t *pool, ul_uniqid_t *ptrid, ul_uniqid_t afterid);
int ul_uniqid_pool_free_one(ul_uniqid_pool_t *pool, ul_uniqid_t id);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_UNIQID_H*/
