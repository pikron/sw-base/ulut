#ifndef _UL_DBUFF_PRINTF_H
#define _UL_DBUFF_PRINTF_H

#include <stdarg.h>
#include <ul_dbuff.h>

int ul_dbuff_printf(ul_dbuff_t *dbuf, const char *format, ...) UL_ATTR_PRINTF (2, 3);;
int ul_dbuff_vprintf(ul_dbuff_t *dbuf, const char *format, va_list ap);

#endif /*_UL_DBUFF_PRINTF_H*/
