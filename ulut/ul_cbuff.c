/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_cbuff.c	- circular buffer

  (C) Copyright 2005-2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.

  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"

#define UL_CBUFF_INCLUDE_INTERNAL

#include "ul_cbuff.h"

int ul_cbuff_init(ul_cbuff_t *buff, ul_cbuff_state_t *state,
		  void *buff_start, ul_cbuff_loc_t size)
{
  ul_cbuff_msg_head_t *msg_head;
  int state_alloc=(state==NULL);

  if(state_alloc){
    state=(ul_cbuff_state_t *)malloc(sizeof(ul_cbuff_state_t));
    if(state==NULL)
      return -1;
  }

  if(buff_start!=NULL){
    size=ul_cbuff_align(size+1)-ul_cbuff_align(1);
  }else{
    size=ul_cbuff_align(size);
    buff_start=malloc(size);
    if(buff_start==NULL){
      if(state_alloc)
        free(state);
      return -1;
    }
  }

  state->buff_size=size;
  state->head=0;
  state->lasttail=0;
  state->cycles=0;
  state->readers=0;
  state->rear_size=0;

  buff->state=state;
  buff->buff_start=(unsigned char *)buff_start;

  msg_head=(ul_cbuff_msg_head_t *)buff_start;
  msg_head->flags=0;

  return 0;
}
