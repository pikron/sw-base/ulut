/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_htimdefault.h  - hierarchical timer compile time default ops

  (C) Copyright 2003-2009 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_htimer.h"

#ifdef UL_HTIMER_WITH_MSTIME

#ifdef UL_HTIMER_WITH_STD_TYPE

extern ul_root_htimer_ops_t ul_mstime_root_htimer_ops;

ul_root_htimer_ops_t *ul_root_htimer_ops_compile_default =
                                      &ul_mstime_root_htimer_ops;

#endif /*UL_HTIMER_WITH_STD_TYPE*/

#endif /*UL_HTIMER_WITH_MSTIME*/
