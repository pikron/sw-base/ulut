/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_hptree.c  - heap tree implementation

  (C) Copyright 2003-2004 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"
#include "ul_hptree.h"

int ul_hpt_init_root_hpt(ul_hpt_root_field_t *hpt_root, int acapacity)
{
  ul_hpt_node_t **ha;
  hpt_root->count=0;
  hpt_root->capacity=acapacity;
  ha=malloc(hpt_root->capacity*sizeof(void *));
  if(!ha){
    hpt_root->heaparr=NULL;
    hpt_root->capacity=0;
    return -1;
  }
  hpt_root->heaparr=ha-1;
  return 0;
}

int ul_hpt_enlarge_hpt(ul_hpt_root_field_t *hpt_root)
{
  ul_hpt_node_t **ha;
  long acap;
  acap=hpt_root->capacity;
  acap=((acap+7)&(~7))*2;
  ha=realloc(hpt_root->heaparr+1,acap*sizeof(void *));
  if(!ha) return -1;  
  hpt_root->capacity=acap;
  hpt_root->heaparr=ha-1;
  return 0;
}

void ul_hpt_done_root_hpt(ul_hpt_root_field_t *hpt_root)
{
  if(hpt_root->heaparr)
    free(hpt_root->heaparr+1);
}
