#include "ul_dbufflog.h"
#include "ul_log.h"

ul_log_domain_t ulogd_dbuff;

void ul_dbuff_log_hex(ul_dbuff_t *buf, int log_level)
{
    int i;
    ul_log(&ulogd_dbuff, log_level, "len=%li capacity=%li\n", buf->len, buf->capacity);
    log_level |= UL_LOGL_CONT;
    for(i=0; i<buf->len; i++) {
        char c = buf->data[i];
        if(i > 0) ul_log(&ulogd_dbuff, log_level, " ");
        if(c > ' ') ul_log(&ulogd_dbuff, log_level, " %c", c);
        else        ul_log(&ulogd_dbuff, log_level, "%02x", c);
    }
    ul_log(&ulogd_dbuff, log_level, "\n");
    for(i=0; i<buf->len; i++) {
        char c = buf->data[i];
        if(i > 0) ul_log(&ulogd_dbuff, log_level, " ");
        ul_log(&ulogd_dbuff, log_level, "%02x", c);
    }
    ul_log(&ulogd_dbuff, log_level, "\n");
}

