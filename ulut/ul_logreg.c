/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_logreg.c	- registration of logging domains

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <ctype.h>
#include <string.h>
#ifndef __RTL__
#include <stdlib.h>
#endif
#include <ul_logbase.h>
#include <ul_logreg.h>
#include <ul_gsacust.h>


typedef struct ul_log_domains_t {
 #ifdef UL_LOG_DOMAINS_STATIC
  /*gsa_static_array_field_t domains;*/
  struct {
    ul_log_domain_t * const* items;
    int count;
  } domains;
 #else /*UL_LOG_DOMAINS_STATIC*/
  gsa_array_field_t domains;
 #endif /*UL_LOG_DOMAINS_STATIC*/
} ul_log_domains_t;

typedef const char *ul_log_domains_key_t;

int
ul_log_domains_cmp_fnc(const ul_log_domains_key_t *a, const ul_log_domains_key_t *b)
{
  return strcmp(*a,*b);
}

/* Custom array declarations */
#ifdef UL_LOG_DOMAINS_STATIC

GSA_STATIC_CUST_DEC(ul_log_domains, ul_log_domains_t, ul_log_domain_t, ul_log_domains_key_t,
	domains, name, ul_log_domains_cmp_fnc)

GSA_STATIC_CUST_IMP(ul_log_domains, ul_log_domains_t, ul_log_domain_t, ul_log_domains_key_t,
	domains, name, ul_log_domains_cmp_fnc, 0)

#else /*UL_LOG_DOMAINS_STATIC*/

GSA_CUST_DEC(ul_log_domains, ul_log_domains_t, ul_log_domain_t, ul_log_domains_key_t,
	domains, name, ul_log_domains_cmp_fnc)

GSA_CUST_IMP(ul_log_domains, ul_log_domains_t, ul_log_domain_t, ul_log_domains_key_t,
	domains, name, ul_log_domains_cmp_fnc, 0)

#endif /*UL_LOG_DOMAINS_STATIC*/

ul_log_domains_t ul_log_domains;

/*This is not ideal*/
extern int ul_log_cutoff_level;

int ul_log_domain_setlevel(const char *name, int setlevel)
{
  int all_fl=0;

  ul_log_domains_it_t it;
  ul_log_domain_t *domain=NULL;

  if(setlevel<0)
    return -1;

  if(setlevel>UL_LOGL_MAX)
    setlevel=UL_LOGL_MAX;

  if(!name)
    all_fl=1;
  else
    all_fl=!strcmp(name,"all") || !strcmp(name,"ALL");

  if(!all_fl){
    domain=ul_log_domains_find(&ul_log_domains,&name);
    if(!domain){
      return 1;
    }
    domain->level=setlevel;
  }else{
    ul_for_each_it(ul_log_domains, &ul_log_domains, it){
      domain=ul_log_domains_it2item(&it);
      domain->level=setlevel;
    }
  }

  return 0;
}

int ul_log_domain_getlevel(const char *name)
{
  ul_log_domain_t *domain=NULL;

  if(!name)
    return -1;

  domain=ul_log_domains_find(&ul_log_domains,&name);
  if(!domain){
    return -1;
  }
  
  return domain->level;
}

#ifndef UL_MAX_DOMAIN_NAME
#define UL_MAX_DOMAIN_NAME 20
#endif /*UL_MAX_DOMAIN_NAME*/

/* Argument syntax:
 *
 * <arg> := <assignment> | <arg> (':' | ',') <assignment>
 * <assignment> := [ <domain> ('.' | '=') ] NUMBER
 * <domain> := ( IDENTIFIER | "all" | "ALL" )
 */
int ul_log_domain_arg2levels(const char *arg)
{
  const char *p=arg;
  const char *r;
  int l;
  char name[UL_MAX_DOMAIN_NAME+1];

  if(!arg)
    return -1;

  while(*p){
    if(isdigit(*p)){
      strcpy(name,"all");
    }else{
      r=p;
      while(isalnum(*p)||(*p=='_')) p++;
      l=p-r;
      if(l>UL_MAX_DOMAIN_NAME)
        l=UL_MAX_DOMAIN_NAME;
      memcpy(name,r,l);
      name[l]=0;
      if(*p&&(*p!='.')&&(*p!='='))
        return p-arg;
      p++;
    }
    r=p;
    l=strtol(r,(char**)&p,0);
    if(!p||(p==r)||(*p&&(*p!=':')&&(*p!=',')))
      return p-arg;
    if(ul_log_domain_setlevel(name, l)<0)
      return p-arg;
    if(*p)
      p++;
  }

  return 0;
}

int ul_logreg_domain(ul_log_domain_t *domain)
{
  ul_log_check_default_output();
  if(!domain->level)
    domain->level=ul_log_cutoff_level;
  return ul_log_domains_insert(&ul_log_domains,domain);
}

int ul_logreg_domains_static(ul_log_domain_t *const *domains, int count)
{
  if(!ul_log_domains.domains.items && !ul_log_domains.domains.alloc_count){
    ul_log_domains.domains.items=(void**)domains;
    ul_log_domains.domains.count=count;
    ul_log_check_default_output();
    while(count-->0){
      if(!(*domains)->level)
         (*domains)->level=ul_log_cutoff_level;
      domains++;
    }
    return 0;
  }
  while(count-->0){
    if(ul_logreg_domain(*(domains++))<0)
      return -1;
  }
  return 0;
}

void ul_logreg_for_each_domain(ul_logreg_domain_cb_t *callback, void *context)
{
  ul_log_domains_it_t it;
  ul_log_domain_t *domain;
  ul_for_each_it(ul_log_domains, &ul_log_domains, it) {
    domain=ul_log_domains_it2item(&it);
    if (callback) {
      int ret;
      ret = callback(domain, context);
      if (ret)
	break;
    }
  }
}
