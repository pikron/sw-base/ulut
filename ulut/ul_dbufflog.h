#ifndef UL_DBUFFLOG_H
#define UL_DBUFFLOG_H

#include "ul_dbuff.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * ul_dbuff_log_hex - writes content of dbuff to log
 * @buf: buffer structure
 * @log_level: logging level
 */
void ul_dbuff_log_hex(ul_dbuff_t *buf, int log_level);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* UL_DBUFFLOG_H */

