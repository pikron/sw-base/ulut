/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_uniqid.h  - unique ID generator

  (C) Copyright 2003-2004 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>

#include "ul_gavl.h"
#include "ul_gavlflesint.h"
#include "ul_uniqid.h"
#include "ul_utmalloc.h"

typedef struct ul_uniqid_pool_item {
  gavl_node_t    node;
  ul_uniqid_range_t range;
} ul_uniqid_pool_item_t;

static inline int
ul_uniqid_pool_cmp_fnc( const ul_uniqid_range_t *a, const ul_uniqid_range_t *b)
{
        if (a->first>b->last) return 1;
        if (a->last<b->first) return -1;
        return 0;
}

GAVL_FLES_INT_DEC(ul_uniqid_pool, ul_uniqid_pool_t, ul_uniqid_pool_item_t, ul_uniqid_range_t,
	items, node, range, ul_uniqid_pool_cmp_fnc)


GAVL_FLES_INT_IMP(ul_uniqid_pool, ul_uniqid_pool_t, ul_uniqid_pool_item_t, ul_uniqid_range_t,
	items, node, range, ul_uniqid_pool_cmp_fnc, GAVL_FANY, , , )


/**
 * ul_uniqid_pool_init - Initialize Unique IDs Pool
 * @pool:	the pointer to the unique IDs pool
 * @first:	the start of the available numeric range
 * @last:	the end of the available numeric range
 *
 * Return Value: The function returns -1 if the @first>@last or if there
 *	is not enough memory to allocate item for initial range representation.
 *	The zero value indicates successful initialization.
 */
int ul_uniqid_pool_init(ul_uniqid_pool_t *pool, ul_uniqid_t first, ul_uniqid_t last)
{
  ul_uniqid_pool_item_t *item;
  
  ul_uniqid_pool_init_root_field(pool);
  
  if(first>last)
    return -1;

  pool->range.first=first;
  pool->range.last=last;

  item=malloc(sizeof(ul_uniqid_pool_item_t));
  if(!item)
    return -1;
  item->range=pool->range;

  ul_uniqid_pool_insert(pool, item);

  return 0;
}

/**
 * ul_uniqid_pool_done - Finalize Unique IDs Pool
 * @pool:	the pointer to the unique IDs pool
 *
 * Return Value: The zero value indicates success.
 */
int ul_uniqid_pool_done(ul_uniqid_pool_t *pool)
{
  ul_uniqid_pool_item_t *item;

  gavl_cust_for_each_cut(ul_uniqid_pool, pool, item){
    free(item);
  }

  return 0;
}

/**
 * ul_uniqid_pool_reserve - Reserve Range from the Unique IDs Pool
 * @pool:	the pointer to the unique IDs pool
 * @first:	the start value of the range
 * @last:	the end value of the range
 *
 * The function checks if specified range @first..@last is free
 * and reserves it from free pool.
 *
 * Return Value: The zero value indicates success. The value of -1 indicates,
 *	that range overlaps with already reserved values or exceeds pool boundaries.
 *	The value 1 is returned in the case, that there is not enough free memory
 *	to represent new non-continuous ranges.
 */
int ul_uniqid_pool_reserve(ul_uniqid_pool_t *pool, ul_uniqid_t first, ul_uniqid_t last)
{
  ul_uniqid_range_t range;
  ul_uniqid_t idsave;
  ul_uniqid_pool_item_t *item;

  range.first=first;
  range.last=last;
  
  if(range.first>range.last)
    return -1;

  item=ul_uniqid_pool_find(pool, &range);
  if(!item)
    return -1;

  if(range.first<item->range.first)
    return -1;

  if(range.last>item->range.last)
    return -1;

  if(range.first==item->range.first){
    if(range.last==item->range.last){
      ul_uniqid_pool_delete(pool, item);
      free(item);
    }else{
      item->range.first=range.last+1;
    }
  }else{
    idsave=item->range.last;
    item->range.last=range.first-1;
    if(range.last!=idsave){
      item=malloc(sizeof(ul_uniqid_pool_item_t));
      if(!item)
	return 1;
      item->range.last=idsave;
      item->range.first=range.last+1;
      ul_uniqid_pool_insert(pool, item);
    }
  }

  return 0;
}


/**
 * ul_uniqid_pool_release - Release Range Back to the Unique IDs Pool
 * @pool:	the pointer to the unique IDs pool
 * @first:	the start value of the range
 * @last:	the end value of the range
 *
 * The range @first..@last is returned to the pool for subsequent reuse.
 *
 * Return Value: The zero value indicates success. The value of -1 indicates,
 *	that range cannot be return back, because there is no free memory
 *	to allocate space for returned range.
 */
int ul_uniqid_pool_release(ul_uniqid_pool_t *pool, ul_uniqid_t first, ul_uniqid_t last)
{
  ul_uniqid_pool_item_t *post, *prev;
  ul_uniqid_range_t range;

  range.first=first;
  range.last=last;
  post=ul_uniqid_pool_find_after(pool,&range);

  if(post){
    prev=ul_uniqid_pool_prev(pool,post);
    if(post->range.first!=range.last+1)
      post=NULL;
    else
      post->range.first=range.first;
  }else{
    prev=ul_uniqid_pool_last(pool);
  }

  if(prev){
    if(prev->range.last!=range.first-1)
      prev=NULL;
    else{
      if(post){
        post->range.first=prev->range.first;
        ul_uniqid_pool_delete(pool, prev);
        free(prev);
      }else{
        prev->range.last=range.last;
      }
    }
  }

  if(!prev && !post){
    post=malloc(sizeof(ul_uniqid_pool_item_t));
    if(!post)
      return -1;
    post->range=range;
    ul_uniqid_pool_insert(pool, post);
  }
  return 0;
}

/**
 * ul_uniqid_pool_alloc_one - Allocate/Generate One Unique ID from the Pool
 * @pool:	the pointer to the unique IDs pool
 * @ptrid:	pointer to ul_uniqid_t variable where unique ID is returned
 *
 * The function allocates lowest available ID from the pool and assigns
 * its value to the space pointed by @ptrid.
 *
 * Return Value: The zero value indicates success. The value of -1 indicates,
 *	that all IDs from the pool are taken. No other reason is possible,
 *	because function does not call any memory allocation function.
 */
int ul_uniqid_pool_alloc_one(ul_uniqid_pool_t *pool, ul_uniqid_t *ptrid)
{
  ul_uniqid_t id;
  ul_uniqid_pool_item_t *item;

  item=ul_uniqid_pool_first(pool);
  if(!item) return -1;
  id=item->range.first;
  if(item->range.first!=item->range.last){
    item->range.first=id+1;
  }else{
    ul_uniqid_pool_delete(pool, item);
    free(item);
  }
  *ptrid=id;
  
  return 0;
}

/**
 * ul_uniqid_pool_alloc_one_after - Allocate One Unique ID Greater Than Value Specified
 * @pool:	the pointer to the unique IDs pool
 * @ptrid:	pointer to ul_uniqid_t variable where unique ID is returned
 * @afterid:	the ID value after which free ID is searched
 *
 * The function allocates the first available ID after @afterid from the
 * pool and assigns its value to the space pointed by @ptrid.
 * If there is no available ID with value greater than @afterid, the first free ID
 * from the whole pool is returned.
 *
 * Return Value: The zero value indicates success. The value of -1 indicates,
 *	that all IDs from the pool are taken. No other reason is possible,
 *	because function does not call any memory allocation function.
 */
int ul_uniqid_pool_alloc_one_after(ul_uniqid_pool_t *pool, ul_uniqid_t *ptrid, ul_uniqid_t afterid)
{
  ul_uniqid_t id;
  ul_uniqid_pool_item_t *item;

  if((afterid>=pool->range.last)||
     (afterid<=pool->range.first)){
    item=NULL;
  } else {
    ul_uniqid_range_t range;
    range.first=range.last=afterid;
    item=ul_uniqid_pool_find_after(pool,&range);
  }

  if(!item)
    item=ul_uniqid_pool_first(pool);

  if(!item)
    return -1;

  id=item->range.first;
  if(item->range.first!=item->range.last){
    item->range.first=id+1;
  }else{
    ul_uniqid_pool_delete(pool, item);
    free(item);
  }
  *ptrid=id;
  
  return 0;
}


/**
 * ul_uniqid_pool_free_one - Release One Previously Allocated Unique ID
 * @pool:	the pointer to the unique IDs pool
 * @id:		the released ID value
 *
 * Return Value: The zero value indicates success. The value of -1 indicates,
 *	that ID cannot be return back, because there is no free memory
 *	to allocate space for range representing returned ID.
 */
int ul_uniqid_pool_free_one(ul_uniqid_pool_t *pool, ul_uniqid_t id)
{
  return ul_uniqid_pool_release(pool, id, id);
}
