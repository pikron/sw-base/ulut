/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_hashtab.h	- hash table declarations

  (C) Copyright 2009 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_HASHTAB_H
#define _UL_HASHTAB_H

#include "ul_utdefs.h"
#include "ul_itbase.h"
#include "ul_gavl.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned int ul_hashtab_hashval_t;

/**
 * struct ul_hastab_node - Structure Representing Node of Generic AVL Tree
 * @treenode:	place to store GAVL node information
 */
typedef struct ul_hashtab_node {
  gavl_node_t treenode;
} ul_hashtab_node_t;

/**
 * struct ul_hashtab_node_hashcache - Structure Representing Node of Generic AVL Tree
 * @treenode:	place to store GAVL node information
 * @hashval:	place to store computed hash value
 */
typedef struct ul_hashtab_node_hashval {
  gavl_node_t treenode;
  ul_hashtab_hashval_t hashval;
} ul_hashtab_node_hashval_t;

typedef struct ul_hashtab_treeroot {
  gavl_cust_root_field_t treeroot;
} ul_hashtab_treeroot_t;

/**
 * struct ul_hashtab_cust_table_field - Structure Representing Hash Table
 * @count:	number of inserted items
 * @hashmask:	mask of bits used to index into actual allocated size
 * @treeroots:	internal storage for hash resolution
 */

typedef struct ul_hashtab_cust_table_field {
  union {
    ul_hashtab_treeroot_t *treetable;
    ul_hashtab_treeroot_t treesingle;
  } treeroots;
  long count;
  ul_hashtab_hashval_t hashmask;
  unsigned char sizestep;
} ul_hashtab_cust_table_field_t;

typedef int (ul_hashtab_resize_insert_fnc_t)(ul_hashtab_cust_table_field_t *table_field, gavl_node_t *treenode) UL_ATTR_REENTRANT;

typedef struct ul_hashtab_sizestep {
  ul_hashtab_hashval_t toshrink;
  ul_hashtab_hashval_t toexpand;
  ul_hashtab_hashval_t size;
} ul_hashtab_sizestep_t;

static inline int ul_hashtab_sizestep_max(const ul_hashtab_sizestep_t *steptab)
{
  return steptab[0].toshrink;
}

extern const ul_hashtab_sizestep_t *ul_hashtab_sizestep_global;
extern const ul_hashtab_sizestep_t * const ul_hashtab_sizestep_default;

#define ul_hashtab_sizestep_null ((ul_hashtab_sizestep_t *)0)

void ul_hashtab_init_table_field_primitive(ul_hashtab_cust_table_field_t *table_field);
int ul_hashtab_delete_all_primitive(ul_hashtab_cust_table_field_t *table_field);
int ul_hashtab_resize_primitive(ul_hashtab_cust_table_field_t *table_field,
		ul_hashtab_hashval_t newsize, ul_hashtab_resize_insert_fnc_t *ins_fnc);
gavl_node_t *ul_hashtab_next_it_primitive(const ul_hashtab_cust_table_field_t *table_field,
	ul_hashtab_treeroot_t **pptreeroot, gavl_node_t *treenode);

/* Declaration of new custom hash table with internal node */
#define UL_HASTAB_CUST_NODE_INT_DEC_SCOPE(cust_scope, cust_prefix, cust_table_t, cust_item_t, cust_key_t,\
		cust_table_field, cust_item_node, cust_item_key, cust_cmp_fnc) \
\
typedef struct {\
  cust_table_t *container; \
  ul_hashtab_treeroot_t *ptreeroot; \
  cust_item_t *item; \
} cust_prefix##_it_t;\
\
cust_scope int cust_prefix##_search_node(const cust_table_t *table, cust_key_t const *key, typeof(((cust_item_t*)0)->cust_item_node) **nodep);\
cust_scope cust_item_t *cust_prefix##_find(const cust_table_t *table, cust_key_t const *key);\
cust_scope int cust_prefix##_insert(cust_table_t *table, cust_item_t *item);\
cust_scope cust_item_t *cust_prefix##_delete_key(cust_table_t *table, cust_key_t const *key);\
cust_scope int cust_prefix##_delete(cust_table_t *table, cust_item_t *item);\
cust_scope int cust_prefix##_resize_table(cust_table_t *table, ul_hashtab_hashval_t newsize);\
cust_scope int cust_prefix##_find_it(cust_table_t *container, cust_key_t const *key, cust_prefix##_it_t *it);\
cust_scope void cust_prefix##_delete_it(cust_prefix##_it_t *it);\
\
static inline \
void cust_prefix##_init_table_field(cust_table_t *table)\
{\
  return ul_hashtab_init_table_field_primitive(&table->cust_table_field);\
} \
\
static inline \
int cust_prefix##_delete_all(cust_table_t *table)\
{\
  return ul_hashtab_delete_all_primitive(&table->cust_table_field);\
} \
\
static inline \
long cust_prefix##_get_count(cust_table_t *table)\
{\
  return table->cust_table_field.count;\
} \
\
static inline \
cust_item_t *cust_prefix##_it2item(const cust_prefix##_it_t *it)\
{\
  return it->item;\
} \
\
static inline int \
cust_prefix##_is_end_it(cust_prefix##_it_t *it)\
{\
  return !it->item;\
} \
\
static inline void \
cust_prefix##_next_it(cust_prefix##_it_t *it) \
{ \
  gavl_node_t *treenode; \
  treenode=ul_hashtab_next_it_primitive(\
    &it->container->cust_table_field, &it->ptreeroot, \
    it->item!=NULL?&it->item->cust_item_node.treenode: NULL); \
  it->item=treenode!=NULL?UL_CONTAINEROF(treenode, cust_item_t, cust_item_node.treenode):NULL;\
} \
\
static inline void \
cust_prefix##_first_it(cust_table_t *container, cust_prefix##_it_t *it)\
{\
  it->container=container;\
  it->item=NULL;\
  cust_prefix##_next_it(it);\
}

#define UL_HASTAB_CUST_NODE_INT_DEC(cust_prefix, cust_table_t, cust_item_t, cust_key_t,\
		cust_table_field, cust_item_node, cust_item_key, cust_cmp_fnc) \
	UL_HASTAB_CUST_NODE_INT_DEC_SCOPE(extern, cust_prefix, cust_table_t, cust_item_t, cust_key_t,\
		cust_table_field, cust_item_node, cust_item_key, cust_cmp_fnc)

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_HASHTAB_H */
