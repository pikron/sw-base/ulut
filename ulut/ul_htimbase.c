/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_htimbase.c	- hierarchical timer base declarations

  (C) Copyright 2003 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>

#define UL_HTIMER_INC_FROM_BASE
#include "ul_htimer.h"

#ifndef UL_HTIMER_WITH_HPTREE
 #include "ul_gavl.h"
 #include "ul_gavlflesint.h"
#else /*UL_HTIMER_WITH_HPTREE*/
 #include "ul_hptree.h"
#endif /*UL_HTIMER_WITH_HPTREE*/

#ifndef UL_HTIMER_WITH_HPTREE

GAVL_FLES_INT_DEC(ul_htim_queue, ul_htim_queue_t, ul_htim_node_t, ul_htim_time_t,
	timers, node, expires, ul_htimer_cmp_fnc)


GAVL_FLES_INT_IMP(ul_htim_queue, ul_htim_queue_t, ul_htim_node_t, ul_htim_time_t,
	timers, node, expires, ul_htimer_cmp_fnc, GAVL_FAFTER, 
	root->first_changed=1,,root->first_changed=1)

#else /*UL_HTIMER_WITH_HPTREE*/

UL_HPTREE_DEC(ul_htim_queue, ul_htim_queue_t, ul_htim_node_t, ul_htim_time_t,
	timers, node, expires, ul_htimer_cmp_fnc)


UL_HPTREE_IMP(ul_htim_queue, ul_htim_queue_t, ul_htim_node_t, ul_htim_time_t,
	timers, node, expires, ul_htimer_cmp_fnc, GAVL_FAFTER, 
	root->first_changed=1,root->first_changed=1)

#endif /*UL_HTIMER_WITH_HPTREE*/
