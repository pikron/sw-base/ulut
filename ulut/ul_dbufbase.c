/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_dbufbase.c	- dynamicaly allocated buffer

  (C) Copyright 2001-2004 by Pavel Pisa - Originator
  (C) Copyright 2003-2004 by Frantisek Vacek - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/
#include <string.h>
#include "ul_utmalloc.h"
#include "ul_dbuff.h"

typedef unsigned char byte;

/**
 * ul_dbuff_init - init memory allocated for dynamic buffer 
 * @buf: buffer structure
 * @flags: flags describing behaviour of the buffer
 *         only UL_DBUFF_IS_STATIC flag is supported.
 *         in this case buffer use unly static array sbuf 
 *
 * Returns capacity of initialised buffer
 */
int ul_dbuff_init(ul_dbuff_t *buf, int flags)
{
    buf->capacity = UL_DBUFF_SLEN;
    buf->data = buf->sbuff;
    buf->len = 0;
    buf->flags = flags;
    return buf->capacity;
}                                                                               

/**
 * ul_dbuff_destroy - frees all resources allocated by buf 
 * @buf: buffer structure
 */
void ul_dbuff_destroy(ul_dbuff_t *buf)
{
    ul_dbuff_prep(buf, 0);
}

/**
 * ul_dbuff_prep - sets a new len and capacity of the buffer
 * @buf: buffer structure
 * @new_len: new desired buffer length
 *
 * Returns new buffer length
 */
int ul_dbuff_prep(ul_dbuff_t *buf, int new_len)
{
    if(buf->flags & UL_DBUFF_IS_STATIC) {
        if(!buf->data || !buf->capacity) {
            buf->capacity = sizeof(buf->sbuff);
            buf->data = buf->sbuff;
        }
        if(new_len > buf->capacity) {
            buf->len=0;
	    return 0;
        }
    }else{
        if(new_len > UL_DBUFF_SLEN) {
            if(new_len != buf->capacity){
                if((buf->data) && (buf->data != &(buf->sbuff[0]))) {
                    free(buf->data);
                }
                buf->data=malloc(new_len);
                if(!buf->data){
                    buf->capacity=0;
                    buf->len=0;
                    return 0;
                }
                buf->capacity=new_len;
            }
        }else{
            if((buf->data) && (buf->data != &(buf->sbuff[0]))) {
                free(buf->data);
                buf->data = &(buf->sbuff[0]);
            }
            buf->capacity=UL_DBUFF_SLEN;
        }
    }
    buf->len = new_len;
    memset(buf->data,0,new_len);
    return buf->len;
}
