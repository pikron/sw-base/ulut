/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_hashtabprim.c	- primitives to support hash tables

  (C) Copyright 2009 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_utmalloc.h"
#include "ul_hashtab.h"

void ul_hashtab_init_table_field_primitive(ul_hashtab_cust_table_field_t *table_field)
{
  table_field->hashmask=0;
  table_field->count=0;
  table_field->treeroots.treesingle.treeroot=NULL;
}

int ul_hashtab_delete_all_primitive(ul_hashtab_cust_table_field_t *table_field)
{
  if(table_field->hashmask)
    free(table_field->treeroots.treetable);
  table_field->sizestep=0;
  table_field->hashmask=0;
  table_field->count=0;
  table_field->treeroots.treesingle.treeroot=NULL;
  return 0;
}

int ul_hashtab_resize_primitive(ul_hashtab_cust_table_field_t *table_field,
		ul_hashtab_hashval_t newsize, ul_hashtab_resize_insert_fnc_t *ins_fnc)
{
  ul_hashtab_hashval_t oldsize=table_field->hashmask+1;
  ul_hashtab_treeroot_t oldsingle;
  ul_hashtab_treeroot_t *pt;
  ul_hashtab_treeroot_t *oldtable;
  gavl_node_t *treenode;
  int res;

  if(newsize<=0)
    newsize=1;

  if(oldsize>1) {
    oldtable=table_field->treeroots.treetable;
  } else {
    oldsingle.treeroot=table_field->treeroots.treesingle.treeroot;
    oldtable=NULL;
  }

  if(newsize>1) {
    pt=malloc(sizeof(*pt)*newsize);
    if(pt==NULL)
      return -1;
    memset(pt,0,sizeof(*pt)*newsize);
    table_field->treeroots.treetable=pt;
  } else {
    table_field->treeroots.treesingle.treeroot=NULL;
  }
  table_field->hashmask=newsize-1;

  pt = oldtable!=NULL? oldtable: &oldsingle;

  while(oldsize--) {
    while(pt->treeroot!=NULL) {
      treenode=gavl_cut_first_primitive(&pt->treeroot);
      res=ins_fnc(table_field, treenode);
    }
    pt++;
  }

  if(oldtable)
    free(oldtable);

  return 0;
}

gavl_node_t *ul_hashtab_next_it_primitive(const ul_hashtab_cust_table_field_t *table_field,
	ul_hashtab_treeroot_t **pptreeroot, gavl_node_t *treenode)
{
  if(treenode!=NULL) {
    treenode=gavl_next_node(treenode);
    if((treenode!=NULL) || !table_field->hashmask)
      return treenode;
    if(table_field->treeroots.treetable+table_field->hashmask==*pptreeroot)
      return treenode;
    (*pptreeroot)++;
  } else {
    if(!table_field->hashmask) {
      const ul_hashtab_treeroot_t *treeroot=&table_field->treeroots.treesingle;
      *pptreeroot=(ul_hashtab_treeroot_t *)treeroot;
    } else {
      *pptreeroot=table_field->treeroots.treetable;
    }
  }
  do {
    if(*pptreeroot) {
      treenode=(*pptreeroot)->treeroot;
      if(treenode)
        while(treenode->left!=NULL)
          treenode=treenode->left;
      if(treenode!=NULL)
        return treenode;
    }
    if(!table_field->hashmask)
      return treenode;
    if(table_field->treeroots.treetable+table_field->hashmask==*pptreeroot)
      return treenode;
    (*pptreeroot)++;
  } while (1);
}

#undef TABENT
#define TABENT(pow) ((ul_hashtab_hashval_t)(1l<<(pow)))

static const ul_hashtab_sizestep_t ul_hashtab_sizestep_default_table[]={
  { ((ul_hashtab_hashval_t)~0l)<(1l<<8)?2:
    ((ul_hashtab_hashval_t)~0l)<(1l<<16)?5:8,
               TABENT( 2), TABENT( 0)},
  {TABENT( 0), TABENT( 4), TABENT( 4)},
  {TABENT( 3), TABENT( 6), TABENT( 6)},
  {TABENT( 5), TABENT( 8), TABENT( 8)},
  {TABENT( 7), TABENT(11), TABENT(10)},
  {TABENT(10), TABENT(14), TABENT(12)},
  {TABENT(13), TABENT(18), TABENT(14)},
  {TABENT(17), TABENT(21), TABENT(16)},
  {TABENT(16),         0,  TABENT(18)},
};

const ul_hashtab_sizestep_t * const ul_hashtab_sizestep_default=ul_hashtab_sizestep_default_table;
const ul_hashtab_sizestep_t *ul_hashtab_sizestep_global=ul_hashtab_sizestep_default_table;
