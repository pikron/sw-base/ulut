#include <stdarg.h>
#include <stdio.h>
#include "ul_dbuff.h"
#include "ul_dbuffprintf.h"

int ul_dbuff_printf(ul_dbuff_t *dbuf, const char *format, ...)
{
  int len;
  va_list ap;
  va_start(ap, format);
  len = ul_dbuff_vprintf(dbuf, format, ap);
  va_end(ap);
  return len;
}

int ul_dbuff_vprintf(ul_dbuff_t *dbuf, const char *format, va_list ap)
{
  int space = dbuf->capacity - dbuf->len;
  int len, pos;
  va_list aq;

  va_copy(aq, ap);
  pos = dbuf->len;
  len = vsnprintf((char*)dbuf->data + pos, space, format, ap);
  if(len >= space) {
    ul_dbuff_set_len(dbuf, pos + len + 2);
    if(dbuf->capacity > pos) {
      space = dbuf->capacity - pos;
      len = vsnprintf((char*)dbuf->data + pos, space, format, aq);
    }
  }
  va_end(aq);
  dbuf->len = pos + len;
  if(dbuf->len > dbuf->capacity)
    dbuf->len = dbuf->capacity;
  return len;
}
