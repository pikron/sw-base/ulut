/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_logbuf.c	- circular log buffer

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#ifndef __RTL__

#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdarg.h>

#else /*__RTL__*/

#include <rtl.h>
#include <string.h>
#include <signal.h>
#include <posix/unistd.h>

#endif /*__RTL__*/

#include "ul_utdefs.h"
#include "ul_logbase.h"

typedef struct ul_log_buff {
  unsigned char *buf_beg;
  unsigned char *buf_end;
  unsigned char *ip;
  unsigned char *op;
} ul_log_buff_t;


ul_log_domain_t *domain
