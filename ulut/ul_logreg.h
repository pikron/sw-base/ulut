/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_logreg.h	- registration of logging domains

  (C) Copyright 2006 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <ul_logbase.h>
#include <ul_utdefs.h>

#ifndef _UL_LOGREG_H
#define _UL_LOGREG_H

#ifdef __cplusplus
extern "C" {
#endif

int ul_log_domain_setlevel(const char *name, int setlevel);
int ul_log_domain_getlevel(const char *name);

int ul_logreg_domain(ul_log_domain_t *domain);
int ul_logreg_domains_static(ul_log_domain_t *const *domains, int count);
int ul_log_domain_arg2levels(const char *arg);

#define UL_LOGREG_DOMAINS_INIT_FUNCTION(function_name, array_name) \
int function_name(void) \
{ \
  int ret; \
  static int domains_registered = 0; \
  if (domains_registered) \
    return 0; \
  ret = ul_logreg_domains_static(array_name, \
           sizeof(array_name)/sizeof(array_name[0])); \
  if (ret >= 0) \
    domains_registered = 1; \
  return ret; \
} \
static void function_name##_on_init(void) UL_ATTR_CONSTRUCTOR; \
static void function_name##_on_init(void) { function_name(); }

#define UL_LOGREG_SINGLE_DOMAIN_INIT_FUNCTION(function_name, domain_name) \
int function_name(void) \
{ \
  int ret; \
  static int domain_registered = 0; \
  if (domain_registered) \
    return 0; \
  ret = ul_logreg_domain(&domain_name); \
  if (ret >= 0) \
    domain_registered = 1; \
  return ret; \
} \
static void function_name##_on_init(void) UL_ATTR_CONSTRUCTOR; \
static void function_name##_on_init(void) { function_name(); }

typedef int (ul_logreg_domain_cb_t)(ul_log_domain_t *domain, void *context);
void ul_logreg_for_each_domain(ul_logreg_domain_cb_t *callback, void *context);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_UL_LOGREG_H*/
