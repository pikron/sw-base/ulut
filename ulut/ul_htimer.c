/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_htimer.c	- hierarchical timer for basic ul_htimer_t type

  (C) Copyright 2003 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_htimer.h"

UL_HTIMER_IMP(ul_htimer, ul_htimer_queue_t, ul_htimer_t, \
	      htim_queue, htim) 

void ul_htimer_run_expired(ul_htimer_queue_t *queue, ul_htim_time_t *pact_time)
{
  ul_htimer_t *timer;
  
  while((timer=ul_htimer_cut_expired(queue, pact_time))){
    UL_HTIMER_FNC_CALL(queue, timer, pact_time);
  }
}

