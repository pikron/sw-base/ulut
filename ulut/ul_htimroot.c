/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_htimroot.h  - hierarchical timer support for root htimer queue

  (C) Copyright 2003-2009 by Pavel Pisa - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#include <string.h>
#include "ul_htimer.h"

#ifdef UL_HTIMER_WITH_STD_TYPE

ul_root_htimer_ops_t *ul_root_htimer_ops;

int ul_root_htimer_add(ul_htimer_t *timer)
{
  int ret;
  ul_htimer_queue_t *root_htimer = ul_root_htimer_get(0, NULL);
  ret = ul_htimer_add(root_htimer, timer);
  ul_root_htimer_put(root_htimer);
  return ret;
}

int ul_root_htimer_detach(ul_htimer_t *timer)
{
  int ret;
  ul_htimer_queue_t *root_htimer = ul_root_htimer_get(0, NULL);
  ret = ul_htimer_detach(root_htimer, timer);
  ul_root_htimer_put(root_htimer);
  return ret;
}

int ul_root_htimer_init(int options, void *context)
{
  if(ul_root_htimer_ops == NULL)
    ul_root_htimer_ops = ul_root_htimer_ops_compile_default;
  return ul_root_htimer_ops->timer_root_init(options, context);
}

#endif /*UL_HTIMER_WITH_STD_TYPE*/
