#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#if !defined(SDCC) && !defined(__SDCC)
#include <sys/time.h>
#endif

#include "ul_utmalloc.h"
#include "ul_hashtab.h"
#include "ul_hashtabcust.h"

typedef int ul_randser_int_t;

#define UL_HASHTAB_WITH_HASHVAL

typedef struct cust3_item {
  int my_val;
 #ifndef UL_HASHTAB_WITH_HASHVAL
  ul_hashtab_node_t my_node;
 #else /*UL_HASHTAB_WITH_HASHCACHE*/
  ul_hashtab_node_hashval_t my_node;
 #endif /*UL_HASHTAB_WITH_HASHCACHE*/
  int more_data;
} cust3_item_t;

typedef struct cust3_table {
  ul_hashtab_cust_table_field_t my_table;
  int my_info;
  int my_first_val;
  int my_last_val;
} cust3_table_t;

typedef int cust3_key_t;

UL_HASTAB_CUST_NODE_INT_DEC(cust3, cust3_table_t, cust3_item_t, cust3_key_t,
	my_table, my_node, my_val, cust3_cmp_fnc)

static inline int
cust3_cmp_fnc(const cust3_key_t *a, const cust3_key_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

static inline ul_hashtab_hashval_t
cust3_hash_fnc(const cust3_key_t *a)
{
  return *a;
}

static inline ul_hashtab_hashval_t
cust3_hash_get_fnc(const cust3_item_t *item)
{
 #ifdef UL_HASHTAB_WITH_HASHVAL
  return item->my_node.hashval;
 #else /*UL_HASHTAB_WITH_HASHCACHE*/
  return cust3_hash_fnc(&item->my_val);
 #endif /*UL_HASHTAB_WITH_HASHCACHE*/
}

static inline void
cust_hash_prep_fnc(cust3_item_t *item)
{
 #ifdef UL_HASHTAB_WITH_HASHVAL
  item->my_node.hashval=cust3_hash_fnc(&item->my_val);
 #endif /*UL_HASHTAB_WITH_HASHCACHE*/
}

UL_HASTAB_CUST_NODE_INT_IMP(cust3, cust3_table_t, cust3_item_t, cust3_key_t,
	my_table, my_node, my_val, cust3_cmp_fnc, cust3_hash_fnc,
	cust3_hash_get_fnc, cust_hash_prep_fnc, ul_hashtab_sizestep_global)


cust3_table_t cust3_table;

ul_randser_int_t *ul_randser_generate(ul_randser_int_t len)
{
  ul_randser_int_t *ser;
  ul_randser_int_t i;

  ser=malloc(sizeof(*ser)*len);
  if(ser==NULL)
    return NULL;

  for(i=0; i<len; i++) {
    ser[i]=i;
  }

  for(i=len; i--; ) {
    ul_randser_int_t t, k;
    k=rand();
    k=(unsigned long long)k*i/RAND_MAX;
    t=ser[k];
    ser[k]=ser[i];
    ser[i]=t;
  }

  return ser;
}

void timing_test_print(struct timeval *start, struct timeval *stop, char *s)
{
  long sec, usec;
  sec=stop->tv_sec-start->tv_sec;
  usec=stop->tv_usec-start->tv_usec;
  if(usec>=1000000) {
    usec-=1000000;
    sec++;
  }
  if(usec<0) {
    usec+=1000000;
    sec--;
  }
  printf("%s :\t%4ld.%06ld\n",s,sec,usec);
}

int main(int argc, char *argv[])
{
  cust3_table_t *table=&cust3_table;
  cust3_item_t *item;
  cust3_key_t k;
  cust3_it_t it;
  int count=100000;
  int tablepow=12;
  cust3_item_t **item_arr;
  struct timeval time_start, time_stop;
  int i, j;
  int res;
  ul_randser_int_t *ser1, *ser2;

  cust3_init_table_field(table);

  if(argc>=2) {
    count=atol(argv[1]);
  }
  if(argc>=3) {
    tablepow=atol(argv[2]);
    ul_hashtab_sizestep_global=ul_hashtab_sizestep_null;
  }

  cust3_resize_table(table, 1<<tablepow);

  ser1=ul_randser_generate(count);
  if(ser1==NULL)
    return 1;

  ser2=ul_randser_generate(count);
  if(ser2==NULL)
    return 1;

  item_arr=malloc(sizeof(*item_arr)*count);
  if(!item_arr)
    return 1;

  for(i=0; i<count; i++) {
    item=malloc(sizeof(*item));
    item->my_val=ser1[i];
    item_arr[i]=item;
  }

  gettimeofday(&time_start,NULL);
  for(i=0; i<count; i++) {
    res=cust3_insert(table, item_arr[i]);
    if(res<=0) {
      printf("cust3_insert %d returned %d\n", i, res);
    }
  }
  gettimeofday(&time_stop,NULL);
  timing_test_print(&time_start,&time_stop,"HASHTAB random insert");

  gettimeofday(&time_start,NULL);
  for(j=0; j<10; j++) {
    for(i=0; i<count; i++) {
      k=ser2[i];
      item=cust3_find(table, &k);
      if(item==NULL) {
        printf("cust3_find %d failed to found item\n", k);
      }
    }
  }
  gettimeofday(&time_stop,NULL);
  timing_test_print(&time_start,&time_stop,"HASHTAB random find 10x");

 #if 0
  for(k=10; (k<count) && (k<20); k++) {
    item=cust3_find(table, &k);
    if(item==NULL) {
      printf("cust3_find %d failed to found item in loop 2\n", k);
      continue;
    }
    if(cust3_delete(table, item)<0)
      printf("cust3_delete failed to found item %d\n", item->my_val);
    else
      free(item);
  }

  for(k=20; k<count; k++) {
    item=cust3_delete_key(table, &k);
    if(item==NULL)
      printf("cust3_delete_key failed to found key %d\n", k);
    else
      free(item);
  }
 #endif

  gettimeofday(&time_start,NULL);
  for(j=0; j<10; j++) {
    i=0;
    ul_for_each_it(cust3, table, it){
      item=cust3_it2item(&it);
      if(item) {
        i++;
      } else {
        printf("ul_for_each_it error\n");
      }
    }
    if(i!=table->my_table.count)
      printf("ul_for_each_it skipped some items\n");
  }
  gettimeofday(&time_stop,NULL);
  timing_test_print(&time_start,&time_stop,"HASHTAB for each by iterator 10x");

  gettimeofday(&time_start,NULL);
  for(cust3_first_it(table,&it); !cust3_is_end_it(&it); ) {
    item=cust3_it2item(&it);
    cust3_delete_it(&it);
    free(item);
  }
  gettimeofday(&time_stop,NULL);
  timing_test_print(&time_start,&time_stop,"HASHTAB delete rest by iterator");

  cust3_delete_all(table);

  free(ser1);
  free(ser2);
  free(item_arr);

  return 0;
}
