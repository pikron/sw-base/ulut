/*******************************************************************
  uLan Utilities Library - C library of basic reusable constructions

  ul_dbuff.c	- dynamic buffer

  (C) Copyright 2001-2004 by Pavel Pisa - Originator
  (C) Copyright 2003-2004 by Frantisek Vacek - Originator

  The uLan utilities library can be used, copied and modified under
  next licenses
    - GPL - GNU General Public License
    - LGPL - GNU Lesser General Public License
    - MPL - Mozilla Public License
    - and other licenses added by project originators
  Code can be modified and re-distributed under any combination
  of the above listed licenses. If contributor does not agree with
  some of the licenses, he/she can delete appropriate line.
  Warning, if you delete all lines, you are not allowed to
  distribute source code and/or binaries utilizing code.
  
  See files COPYING and README for details.

 *******************************************************************/

#ifndef _UL_DBUFF_H
#define _UL_DBUFF_H

#include "ul_utdefs.h"

#ifdef __cplusplus
extern "C" {
#endif

#define UL_DBUFF_SLEN 8

#define UL_DBUFF_IS_STATIC 1 
/* use only sbuf or reserved data space (no dynamic allocation) */

/**
 * struct ul_dbuff - Generic Buffer for Dynamic Data
 * @len:	actual length of stored data
 * @capacity:	capacity of allocated buffer
 * @flags:	only one flag (%UL_DBUFF_IS_STATIC) used now
 * @data:	pointer to dynamically allocated buffer
 * @sbuff:	static buffer for small data sizes
 */
typedef struct ul_dbuff {	/* generic buffer for dynamic data */
  unsigned long len;		/* actual length of stored data */
  unsigned long capacity;	/* capacity of allocated buffer */
  int flags;                /* only one flag (UL_DBUFF_IS_STATIC) exists*/
  unsigned char *data;		/* pointer to dynamically allocated buffer */
  unsigned char	sbuff[UL_DBUFF_SLEN]; /* static buffer */
} ul_dbuff_t;

/* Basic routines */

int ul_dbuff_init(ul_dbuff_t *buf, int flags);
void ul_dbuff_destroy(ul_dbuff_t *buf);
int ul_dbuff_prep(ul_dbuff_t *buf, int new_len);

/* Extended set of routines */

int ul_dbuff_set_capacity(ul_dbuff_t *buf, int new_capacity);
int ul_dbuff_set_len(ul_dbuff_t *buf, int new_len);
int ul_dbuff_set(ul_dbuff_t *buf, unsigned char b, int len);
int ul_dbuff_cpy(ul_dbuff_t *buf, const void *b, int len);
int ul_dbuff_cat(ul_dbuff_t *buf, const void *b, int len);
int ul_dbuff_pos(const ul_dbuff_t *buf, unsigned char what, unsigned char quote);
int ul_dbuff_strcpy(ul_dbuff_t *buf, const char *str);
int ul_dbuff_strcat(ul_dbuff_t *buf, const char *str);
int ul_dbuff_append_byte(ul_dbuff_t *buf, unsigned char b);
int ul_dbuff_export(ul_dbuff_t *srcdb, void *dest, int maxlen);

void ul_dbuff_cut_pos(ul_dbuff_t *fromdb, ul_dbuff_t *todb, int n);
void ul_dbuff_cut_delimited(ul_dbuff_t *fromdb, ul_dbuff_t *todb, char delimiter, char quote);
void ul_dbuff_cut_token(ul_dbuff_t *fromdb, ul_dbuff_t *todb);

/*
 * ul_dbuff_ltrim - removes all white spaces from left of dbuff.
 *
 * Return Value: new len of dbuff (including terminating zero, if present)
 */
int ul_dbuff_ltrim(ul_dbuff_t *buf);
/*
 * ul_dbuff_rtrim - removes all white spaces from right of dbuff.
 *                  If dbuff is zero terminated, trimmed dbuff is also zero terminated
 *
 * Return Value: new len of dbuff (including terminating zero, if present)
 */
int ul_dbuff_rtrim(ul_dbuff_t *buf);
/*
 * ul_dbuff_trim - removes all white spaces from left and right of dbuff.
 *                 If dbuff is zero terminated, trimmed dbuff is also zero terminated
 *
 * Return Value: new len of dbuff (including terminating zero, if present)
 */
int ul_dbuff_trim(ul_dbuff_t *buf);

void ul_dbuff_log_hex(ul_dbuff_t *buf, int log_level);

int ul_str_pos(const unsigned char *str, const unsigned char *what, unsigned char quote);
int ul_str_cpos(const unsigned char *str, unsigned char what, unsigned char quote);
int ul_str_ncpy(unsigned char *to, const unsigned char *from, int buff_size);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _UL_DBUFF_H */
